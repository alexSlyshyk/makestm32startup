/*******************************************************************************
*  file    :
*  created : 16.02.2015
*  author  :
*******************************************************************************/

#ifndef PARSER_HELPER_HPP
#define PARSER_HELPER_HPP

#include <QList>
#include <QString>
#include <QStringList>

class FunctionNote
{
public:
    QString name;
    int zeroVal;
    FunctionNote()
    {
        this->zeroVal = 0;
        this->name.clear();
    }
    FunctionNote(int val)
    {
        this->zeroVal = val;
        this->name.clear();
    }
    FunctionNote(QString name)
    {
        this->name = name;
        this->zeroVal = 0;
    }
};

void addFunctionList(QString nxt, QList<FunctionNote> *functionList);

#endif // PARSER_HELPER_HPP
