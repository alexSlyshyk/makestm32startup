
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QCoreApplication>
#include <QCommandLineParser>
#include "parser_helper.hpp"

extern char startup_template[];

QTextStream out(stdout);

int main(int argc, char* argv[])
{
    // Проверяю количество аргументов
    QCoreApplication a(argc, argv);
    int              verbosity = 0;
    if (argc < 2) {
        out << "ERROR: You mast show me the input file" << Qt::endl;
        return 1;
    }
    for (int i = 1; i < a.arguments().size(); ++i) {
        if (a.arguments().at(i) == "-h") {
            out << "Programm for convert asm startup for C startup for stm32 "
                   "MCUs."
                << Qt::endl;
            out << "-h  - help. This message." << Qt::endl;
            out << "-v  - verbose. Enable some debug outputs." << Qt::endl;
            out << "[filename] - last argument must be a asm startup for stm32 "
                   "MCU."
                << Qt::endl;
            return EXIT_SUCCESS;
        } else if (a.arguments().at(i) == "-v") {
            verbosity = 1;
        }
    }
    // Пытаюсь открыть файл
    QString filename = a.arguments().at(a.arguments().size() - 1);
    QFile   asmFile(filename);
    if (!asmFile.open(QFile::ReadOnly)) {
        out << "ERROR: Can't open file: " << filename << " ["
            << asmFile.errorString() << "]" << Qt::endl;
        return 1;
    }
    // Отматываю заголовок
    QString  mcuName;
    uint32_t bootRAM = 0;
    while (!asmFile.atEnd()) {
        QString nxt = asmFile.readLine().simplified();
        int     p   = nxt.indexOf("@file");
        if (p > 1) {
            mcuName = nxt.mid(p + 5).simplified();

            if (mcuName.endsWith(".s", Qt::CaseInsensitive)) {
                mcuName.chop(2);
            }
        }
        if (nxt == ".word _estack") {
            break;
        }
        if (nxt.contains("BootRAM")) {
            QStringList l = nxt.simplified().split(" ");
            if (l.size() > 2)
                bootRAM = l.at(2).toUInt(0, 0);
        }
    }
    // Разбираю список функций
    QList<FunctionNote> functionList;
    while (!asmFile.atEnd()) {
        QString nxt = asmFile.readLine().simplified();
        if (nxt.size() == 0)
            continue;
        if (nxt.startsWith("/*"))
            continue;
        if (nxt.startsWith(".word ")) {
            addFunctionList(nxt, &functionList);
        } else
            break;
    }
    asmFile.close();

    // Формирую выход
    QString headerList;
    QString headerPrototypes;
    QString cppFill;
    QString cppPragma;

    QString startup = QString(startup_template);

    for (const auto& note: functionList) {
        if (note.zeroVal) {// Zero
            cppFill += "    0";
            for (int i = 1; i < note.zeroVal; i++) cppFill += ", 0";
            cppFill += ",\n";
        } else {
            QString space = QString(30 - note.name.size(), ' ');
            // header
            if (!note.name.contains("BootRAM")) {
                headerList += "            void (*" + note.name + ")" + space +
                              "(void);\n";
                headerPrototypes +=
                    "void " + note.name + " " + space + "(void);\n";
                // cpp
                cppFill += "    " + note.name + ",\n";
                cppPragma += "#pragma weak " + note.name + space +
                             " = Default_Handler \n";
            } else {
                cppFill += "    (intfunc)BootRAM \n";
            }
        }
    }

    startup = startup.arg(bootRAM, 0, 16)
                  .arg(headerPrototypes)
                  .arg(cppFill)
                  .arg(cppPragma);
    // write output files
    QFile outFile;

    outFile.setFileName(mcuName + QString(".c"));
    outFile.open(QIODevice::WriteOnly);
    outFile.write(startup.toLatin1());
    outFile.close();

    if (verbosity) {
        qDebug() << headerList << headerPrototypes << cppFill << cppPragma;
    }

    return 0;
}
