

char startup_template [] =

"extern unsigned long _sidata;\n"
"extern unsigned long _sdata;\n"
"extern unsigned long _edata;\n"
"extern unsigned long _sbss;\n"
"extern unsigned long _ebss;\n"
"extern unsigned long _estack;\n"
"extern unsigned long __ctors_start__;\n"
"extern unsigned long __ctors_end__;\n\n"

"#define BootRAM (0x%1)\n\n"

"void Reset_Handler  (void) __attribute__((__interrupt__));\n"
"void Default_Handler(void);\n"
"void __Init_Data    (void);\n\n"

"extern int  main(void);\n"
"extern void SystemInit(void);\n"
"extern void SystemCoreClockUpdate(void);\n\n"

"/*Interrupts headers*/\n"
"%2\n\n"

"/******************************************************************************\n"
"*                              Vector table                                   *\n"
"******************************************************************************/\n"
"typedef void( *const intfunc )( void );\n\n"

"__attribute__ ((used))\n"
"__attribute__ ((section(\".isr_vector\")))\n"
"void (* const g_pfnVectors[])(void) = {\n"
"    (intfunc)((unsigned long)&_estack),\n"
"/*Interrupts*/\n"
"%3\n};\n\n"

"void Reset_Handler()\n"
"{\n"
"    __Init_Data();\n"
"    main();\n"
"}\n\n"

"void __Init_Data(void)\n"
"{\n"
"    unsigned long *pulSrc, *pulDest;\n"
"    pulSrc = &_sidata;\n"
"    for(pulDest = &_sdata; pulDest < &_edata; )\n"
"        *(pulDest++) = *(pulSrc++);\n"
"    for(pulDest = &_sbss; pulDest < &_ebss; )\n"
"        *(pulDest++) = 0;\n"
"    /* Init hardware before calling constructors */\n"
"    SystemInit();\n"
"    SystemCoreClockUpdate();\n"
"    /* Call constructors */\n"
"    unsigned long *ctors;\n"
"    for(ctors = &__ctors_start__; ctors < &__ctors_end__; )\n"
"        ((void(*)(void))(*ctors++))();\n"
"}\n\n"

"/*Weak aliases for each exception handler*/\n"
"%4\n"


"void Default_Handler(void){  for (;;);}\n"

;

