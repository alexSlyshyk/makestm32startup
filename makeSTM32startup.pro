#-------------------------------------------------
#
# Project created by QtCreator 2014-11-08T19:19:37
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = convstartup
CONFIG   += console
CONFIG   -= app_bundle
CONFIG += c++11


TEMPLATE = app


DESTDIR  = ./bin

UI_HEADERS_DIR = build
RCC_DIR        = build

CONFIG(debug, debug|release) {
OBJECTS_DIR = build/debug
MOC_DIR     = build/debug
}
else {
OBJECTS_DIR = build/release
MOC_DIR     = build/release
}

SOURCES += src/main.cpp \
    src/startup_template.cpp \
    src/parser_helper.cpp

HEADERS += \
    src/parser_helper.hpp
