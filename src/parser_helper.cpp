/*******************************************************************************
 *  file    :
 *  created : 16.02.2015
 *  author  :
 *******************************************************************************/

#include "parser_helper.hpp"

void addFunctionList(QString nxt, QList<FunctionNote>* functionList)
{
    static int  zero = 0;
    QStringList s    = nxt.split(" ");
    QString     n;
    if (s.size() > 1) {
        n = s.at(1);
    } else {
        return;
    }
    if (n.startsWith("0")) {
        zero++;
        return;
    }
    if (zero) {
        functionList->append(FunctionNote(zero));
        zero = 0;
    }

    if (n == "SysTick") {
        n = "SysTickTimer";
    }

    functionList->append(FunctionNote(n));
}
